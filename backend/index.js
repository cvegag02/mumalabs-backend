//import express from "express";empleamos ECMA6
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");

const app = express();
app.unsubscribe(morgan("dev"));

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(
  express.json({
    extended: false,
  })
);

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

const db = require("./models");

db.sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
    db.sequelize.sync();
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
  });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to  Mumalabs  Backend application." });
});

require("./routes/api/usuario.routes")(app);
require("./routes/api/userContainer.routes")(app);
require("./routes/api/login.routes")(app);
require("./routes/api/container.routes")(app);
require("./routes/api/image.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});


setInterval(function(){ db.sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
  }); }, (30*60*1000));