const token = require('../services/token');
const db = require("../models");

const Usuario = db.usuarios;
const UserContainer = db.userContainers;
const Container = db.containers;
const Image = db.images;
exports.verifyUser = async (req,res,next) => {
    // token del header
    try{
        if (!req.headers.token){
            return res.status(404).send({
                message: "Wrong Token, Authentication denied",
            });
        }
        user = await Usuario.findByPk(req.params.userid)
        const response=await token.decodeSimple(req.headers.token).then((a)=>{
            if(a._id==user.dataValues.id){
                next();
            } else {
                return res.status(404).send({
                    message: "Authentication denied",
                });
            }
        })      
    }catch(e){
        return res.status(404).send({
            message: "Authentication denied",
        });
    }
}
