const jwt = require("jsonwebtoken");
const db = require("../models");
const Usuario = db.usuarios;
const secretKey = "secretKey"

//esta funcion es para cuando pase 1 dia checkear el token
async function checkToken(token){
    let __id = null;
    try{
        const myToken= await jwt.decode(token);
        __id = myToken._id;
    } catch (e){
        return false;
    }
    const user = await Usuario.findByPk(__id);
    if (user){
        return encode(user.id,user.name);
    } else {
        return false;
    }
}

exports.encode=async (id,username) => {
    const token = jwt.sign({_id:id,_username:username},secretKey,{expiresIn: '1d'});
    return token;
},

exports.decodeSimple= async (token) =>{
    const myToken = await jwt.verify(token,secretKey);
    return myToken
},

exports.decode= async (token,username) => {
    try {
           
        const myToken = await jwt.verify(token,secretKey);
         
        const user = await Usuario.findByPk(myToken._id);
            
        if (user.name==username){
            return token;
        } else{
            return false;
        }
    } catch (e){
            
        checkToken(token).then(
            (newToken)=>{
                jwt.verify(newToken,secretKey)
            }

        ).then((userNew)=>{
            Usuario.findByPk(userNew._id)
        })
        .then((user)=>{
            if (user.name==username){
                return newToken;
            } else{
                return false;
            }
        }).catch(()=>{
            return false;
        })
    }
}
