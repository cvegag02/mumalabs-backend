const db = require("../models");
const Usuario = db.usuarios;
const UserContainer = db.userContainers;
const Container = db.containers;
const Image = db.images;

const bcrypt = require("bcryptjs");

const Op = db.Sequelize.Op;

// Create and Save a new Usuario
exports.create = async (req, res) => {
  // Validate request

  if (!req.body.name | !req.body.password | !req.body.email) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  try {
    const user = await Usuario.findOne({
      where: { name: req.body.name },
      attributes: { exclude: ["password"] },
    });

    if (user) {
      return res
        .status(500)
        .json({ msg: "Error 404! -> A user with the same name already exists" });
    }

    const salt = await bcrypt.genSalt(10);
    password = await bcrypt.hash(req.body.password, salt);

    // Create a USER
    const usuario = {
      name: req.body.name,
      email: req.body.email,
      password: password,
      rol:0,
    };

    // Save User in the database
    await Usuario.create(usuario).then((usuario) => {
      res.json({
        usuario,
      });
    });
  } catch (exception) {
    console.error(exception.message);
    res.status(500).send("Server ERROR!");
  }
};



//----------------------NOT Tested ----------------------------------------

// Retrieve all Usuarios by name from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  Usuario.findAll({ where: condition })
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving tutorials.",
    });
  });
};

// Find a single Usuario with a id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Usuario.findByPk(id)
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error retrieving Tutorial with id=" + id,
    });
  });
};

// Update a User by the name in the request
exports.update = (req, res) => {
  const name = req.params.name;

  Usuario.update(req.body, {
    where: { name: name },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "Tutorial was updated successfully.",
      });
    } else {
      res.send({
        message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error updating Tutorial with id=" + id,
    });
  });
};

// Delete a Tutorial with the specified name in the request
exports.delete = (req, res) => {
  const name = req.params.name;

  Usuario.destroy({
    where: { name: name },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "User was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Could not delete Tutorial with id=" + id,
    });
  });
};

// Delete all Usuarios from the database.
exports.deleteAll = (req, res) => {
  Usuario.destroy({
    where: {},
    truncate: false,
  })
  .then((nums) => {
    res.send({ message: `${nums} Tutorials were deleted successfully!` });
  })
  .catch((err) => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while removing all tutorials.",
    });
  });
};
