const Dockerode = require("dockerode");
const db = require("../models");
const UserContainer = db.userContainers;
const Usuario = db.usuarios;
const Container = db.containers;
const Image = db.images;

const dock = require("../controllers/docker");
const { containers } = require("../models");





const Op = db.Sequelize.Op;

// Retrieve all UserContainer by name from the database.
exports.findAll = (req, res) => {

  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  UserContainer.findAll({ where: condition })
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving tutorials.",
    });
  });
};

// Find a single Usuario with a name
exports.findOne = (req, res) => {
  const id = req.params.id;
  
  UserContainer.findByPk(id)
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error retrieving UserContainer with id=" + id,
    });
  });
};
 
// Update an UserContainer by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  //COMPROBAR
  
  Container.findByPk(id)
  .then((container) => {
    if(container.dataValues.status == "Exited"){
      dock.start(container.dataValues.name);
    } else{
      dock.stop(container.dataValues.name);
    }
    
  });
  
  UserContainer.update(req.body, {
    where: { id: id },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "Container was updated successfully.",
      });
    } else {
      res.send({
        message: `Cannot update Container with id=${id}. Maybe UserContainer was not found or req.body is empty!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error updating Container with id=" + id,
    });
  });

  Container.update(req.body, {
    where: { id: id },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "Container was updated successfully.",
      });
    } else {
      res.send({
        message: `Cannot update Container with id=${id}. Maybe UserContainer was not found or req.body is empty!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error updating Container with id=" + id,
    });
  });
};

// Delete a UserContainer with the specified name in the request
exports.delete = (req, res) => {
  

  const id = req.params.id;
  var userId



  UserContainer.findByPk(id)
  .then((userContainer)=>{
    userId = userContainer.usuarioId
    UserContainer.destroy({
      where: { id: id },
    })
    .then(
      (num) => {
        if (num == 1) {
          UserContainer.findAll({where:{ usuarioId: userId }})
          .then((userContainers)=>{
            res.json(userContainers)
          })
        } else {
          res.send({
            message: `Cannot delete UserContainer with id=${id}. Maybe UserContainer was not found!`,
          });
        }
      }
    )
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete UserContainer with id=" + id,
      });
    })
  })

  const idd = req.params.id;

  /* Delete container */
  Container.findByPk(idd)
              .then((container) => {
                dock.delete(container.dataValues.name);
              });



  Container.destroy({
    where: { id: idd },
  })
  .then((num) => {
    if(num == 1) {
      res.send({
        message: "Container was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`,
      });
    } 
  })
  .catch((err) => {
    res.status(500).send({
      message: "Could not delete UserContainer with id=" + id,
    });
  });
};



exports.getUserContainersByUser = (req, res) => {
  const userid = req.params.userid;


  UserContainer.findAll({ where: { usuarioId: userid } }).then(containers => {
    res.json(containers)
  })
};



exports.setUserContainerByUser = async (req, res) => {

  const userid = req.params.userid;
  

  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  
  /* First the container is created */
  const image = await Image.findByPk(req.body.imageID)
  if (!image) {
    res.status(400).send({
      message: "Imagen no existe!",
    });
  }

  try {
    const containerName = await Container.findOne({
      where: { name: req.body.name },
    });


    if (containerName) {
      return res
        .status(500)
        .json({ msg: "Error 404! -> A Container with the same name already exists" });
    }

    // Create a Container
    const container = {
      name: req.body.name,
      status: req.body.status,
      time: req.body.time,
      imageId: req.body.imageID
    };

    /* Create a docker Container  */
    dock.create(image.name, req.body.name, "bridge");
    


    var containerIDD = 0;
    // Save Container in the database
    await Container.create(container).then((container) => {
      res.json({
        container,
      });
      containerIDD = container.id;
    });
    
  } catch (exception) {
    console.error(exception.message);
    res.status(500).send("Server Error!");
  }

  /* Created the container, the user's container is created */
  if (!containerIDD) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const user = await Usuario.findByPk(userid)
  if(!user){
    res.status(400).send({
      message: "Usuario no existe!",
    });
  }

  try {
    // Create a UserContainer
    const userContainer = {
      containerId: containerIDD,
      usuarioId: userid
    };

    // Save UserContainer in the database
    await UserContainer.create(userContainer).then((userContainer) => {
      UserContainer.findAll({where:{ usuarioId: userContainer.usuarioId }})
      .then((userContainers)=>{res.json(userContainers)})
      
    });
    
  } catch (exception) {
    console.error(exception.message);
    res.status(500).send("Serveree ERROR!");
  }
};
