var Docker = require('dockerode');
const { containers } = require('../models');
var docker = new Docker({socketPath: '/var/run/docker.sock'});

const db = require("../models");
const Container = db.containers;

function printCorrectFormatDate(dateSeconds) {
    dateSeconds = parseInt(dateSeconds)
    if (dateSeconds < 60) {
        return `${dateSeconds} seconds ago`
    }
    else if (dateSeconds < 3600) {
        minutes = parseInt(dateSeconds / 60)
        seconds = (dateSeconds - minutes * 60) 
        return `${minutes} minutes and ${seconds} seconds ago`
    }
    else {
        hours = parseInt(dateSeconds / 3600)
        minutes = parseInt((dateSeconds - hours * 3600) / 60)
        seconds = (dateSeconds - hours * 3600 - minutes * 60) 
        return `${hours} hours, ${minutes} minutes and ${seconds} seconds ago` 
    }
}



exports.update = function getUpdatesContainers(containerNames, id) {
    console.log("dentro del update")


    let promises = []
    //for (let containerName of containerNames) {
        /* Cambie  containerName por containerNames en el push */
        promises.push(inspectContainer(containerNames))
    //}
    
    


    Promise.all(promises).then(data => {
        clean = data.map(item => {
            
            let date = new Date(item.State.Status == "running" ? item.State.StartedAt : item.State.FinishedAt)
            if (item.State.Status == "running") {
                estado = "Up"
            }
            body = {
                "name": item.Name.substring(1),
                
                "status": estado,
                "time": printCorrectFormatDate(Math.abs(Date.now() - date)/1000)
            }
            /* Actualización del contenedor de la base de datos */
            Container.update(
                body, {
                where: { id: id },
            })
        })
        console.log(clean)
    })
    .catch(err => console.log(err))

    
}



// https://docs.docker.com/engine/api/v1.40
exports.create = function createNewContainer(image, name, network_name) {
    docker.createContainer({Image: image, name: name},  (err, container) => {
        if (err) {
            console.log("ERROR CREATING CONTAINER")
        }
        let network = docker.getNetwork(network_name)
        network.connect({"Container": container.id}, (err, data) => {
            if (err) {
                console.log("ERROR ATTACHING TO CONTAINER")
            }
        })
        container.start( (err, data) => {
            if (err) {
                console.log("ERROR STARTING CONTAINER")
            }        
        })
    })
}

exports.delete = function deleteContainer(name) {
    let container = docker.getContainer(name)
    container.stop((err, data) => {
        if (err) {
            console.log("ERROR STOPPING CONTAINER")
        }

        container.remove((err, data) => {
            if (err) {
                console.log("ERROR DELETING CONTAINER")
            }
        })
    })
}

exports.start = function startContainer(name) {
    let container = docker.getContainer(name)
    container.start((err, data) => {
        if (err) {
            console.log("ERROR STARTING CONTAINER")
        }
    })
}


exports.stop = function stopContainer(name) {
    let container = docker.getContainer(name)
    container.stop((err, data) => {
        if (err) {
            console.log("ERROR STOPPING CONTAINER")
        }
    })
}


async function inspectContainer(name) {
    
    let container = docker.getContainer(name)
    return container.inspect()
}
