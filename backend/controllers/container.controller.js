const db = require("../models");
const Usuario = db.usuarios;
const UserContainer = db.userContainers;
const Container = db.containers;
const Image = db.images;

const bcrypt = require("bcryptjs");

const Op = db.Sequelize.Op;

const dock = require("../controllers/docker");

// Update an Container by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Container.update(req.body, {
    where: { id: id },
  })
  .then((num) => {
    if (num == 1) {
      Apuesta.findAll({where:{ usuarioId: userId }})
      .then((apuestas)=>{
        res.json(apuestas)})
    } else {
      res.send({
        message: `Cannot update Container with id=${id}. Maybe Container was not found or req.body is empty!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error updating Container with id=" + id,
    });
  });
};


// Create and Save a new Container
exports.create = async (req, res) => {
  // Validate request

  const image = await Image.findByPk(req.body.imageID)
  if (!image) {
    res.status(400).send({
      message: "Imagen no existe!",
    });
  }

  

  try {
    // Create a Container
    const container = {
      name: req.body.name,
      status: req.body.status,
      imageId: req.body.imageID,
      time: req.body.time
    };
  
    // Save Container in the database
    await Container.create(container).then((container) => {
      res.json({
        container,
      });
    });
    
  } catch (exception) {
    console.error(exception.message);
    res.status(500).send("Server Error!");
  }
};

  
// Retrieve all Containers by name from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
  
  Container.findAll({ where: condition })
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving tutorials.",
      });
  });
};




// Find a single Usuario with a name
exports.findOne = (req, res) => {
  const id = req.params.id;

  console.log("DENTROOOOO")
  console.log(id)

  Container.findByPk(id)
              .then((container) => {
                console.log("INTENTO")
                console.log(dock.update(container.dataValues.name, id));
              });

  Container.findByPk(id)
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error retrieving container with id=" + id,
    });
  });
};
  
  
  
// Delete
exports.delete = (req, res) => {
  const name = req.params.name;
  
  Usuario.destroy({
    where: { name: name },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "User was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Could not delete Tutorial with id=" + id,
    });
  });
};
  
// Delete all Usuarios from the database.
exports.deleteAll = (req, res) => {
  Usuario.destroy({
    where: {},
    truncate: false,
  })
  .then((nums) => {
    res.send({ message: `${nums} Tutorials were deleted successfully!` });
  })
  .catch((err) => {
    res.status(500).send({
      message:
      err.message || "Some error occurred while removing all tutorials.",
    });
  });
};
  